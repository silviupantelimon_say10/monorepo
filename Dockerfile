FROM gcc:11.2

RUN apt-get update -y
RUN apt-get install -y cmake
RUN apt-get install -y ssh
RUN apt-get install -y git
RUN apt-get install -y gdb
RUN apt-get install -y doxygen
RUN apt-get install -y graphviz
RUN apt-get install -y plantuml