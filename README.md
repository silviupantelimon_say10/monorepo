Project structure:
    1: saytenlib - a library for high level functional programming in C++20
    2: yoneda - an application that will be used as a code generator

Running the development environment:
    1. docker-compose up -d --build
    2. use Visual Studio Code to connect into the container in the folder "/opt/dev"

To run saytenlib tests either:
    * use Visual Studio Code with extensions to run individual tests
    * run build.sh and test.sh scripts
    * use cmake and ctest on saytenlib

To run yoneda tests either:
    * use Visual Studio Code with extensions to run the executable
    * run build.sh and run.sh scripts
    * use cmake on yoneda and run the "generator" executable